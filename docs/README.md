{% include header.md %}

## Using this portal page
This portal offers a gateway to the *services* and some additional *user documentation* of Development Services.

:book: Click the *book* button for documentation of a service.  
:arrow_forward: Click the *arrow* button for the actual service. Opens in a new window.

## Development Services

### [:book: Portal ](userpages/portal/README.md "Portal documentation") [:arrow_forward:](https://dev.kadaster.nl/portal "Legacy portal"){:target="_blank"} 
The former Development Services portal. Will be replaced by the portal you are looking at soon.

### [:book: Github](userpages/github/README.md "Github documentation") [:arrow_forward:](https://github.com/enterprises/kadaster "Github Cloud"){:target="_blank"}
GitHub is a code hosting platform for version control and collaboration.  

### [:book: Jira](userpages/jira/README.md "Jira documentation") [:arrow_forward:](https://dev.kadaster.nl/jira "Jira"){:target="_blank"}
Jira is a flexible issue tracking tool that helps teams plan, manage, and report on their work. 

### [:book: Jenkins](userpages/jenkins/README.md "Jenkins documentation") [:arrow_forward:](userpages/jenkins/instances.md "Jenkins switchboard")

Jenkins is an open source automation server that helps automate the parts of software development related to building, testing, and deploying, facilitating continuous integration and continuous delivery. 

### [:book: Artifactory](userpages/artifactory/README.md "Artifactory documentation") [:arrow_forward:](https://dev.kadaster.nl/artifactory "Artifactory"){:target="_blank"}

Artifactory is a universal DevOps solution that offers management and caching of binaries and artifacts throughout the application delivery process.

### [:book: SonarQube](userpages/sonarqube/README.md "SonarQube documentation") [:arrow_forward:](https://dev.kadaster.nl/sonar "SonarQube"){:target="_blank"}

SonarQube empowers all developers to write cleaner and safer code and supports Continuous Code Inspection.

### [:book: Miscellaneous Development Services](userpages/miscellaneous/README.md "Miscellaneous documentation")
Team DevForce also offers some other services that may be of your interest.

## Other Services maintained by DevForce
### PLS 
### KIM
### TBO
### SUSE

## External Services

### [:book: PLS Azure Cloud Selfservice (Kadaster)](https://pls.test.cloud.kadaster.nl/wiki/products/ "PLS documentation"){:target="_blank"} [:arrow_forward:](https://pls.kadaster.nl/selfservice/){:target="_blank"}
The Kadaster Azure Cloud Platform Services team (PLS) has created an abstraction layer for Azure services to ease Azure Cloud development.

### [:book: PLP Internal Cloud Selfservice (Capgemini)](https://documentatie.plp.kadaster.nl/ "PLP documentation"){:target="_blank"} [:arrow_forward:](https://portal.plp.kadaster.nl/){:target="_blank"}
The Capgemini PLP team delivers proprietatry cloud platform solutions that will be replaced by the PLS platform services in the future.

### [:book: PLP Splunk Monitoring (Capgemini)](https://documentatie.plp.kadaster.nl/docs/architectuur/monitoringenlogging/ "Splunk documentation"){:target="_blank"} [:arrow_forward:](https://monitoring.plp.kadaster.nl/){:target="_blank"}
The Capgemini PLP team maintains the Splunk monitoring solution that is integrated with the PLP platform.

{% include footer.md %}
