{% include admin-header.md %}

## Artifactory administration and usage

### [Switch production and standby instance](switch-to-standby.md)
The Artifactory setup has a production and a standby instance, which may be switched when necessary.

{% include footer.md %}
