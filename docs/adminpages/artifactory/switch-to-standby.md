{% include admin-header.md %}

## Switch Artifactory production and standby instance

This is a description of the actions to be performed when the Artifactory Production instance is not usable anymore, or there are other reasons to switch the production and standby instances.

### Switching instructions

The following action need to be performed to switch the production and standby instances:

* **Disable replication** by running the Jenkins job `dev-artifactory-delete-replicationconfig`.
* Goto the PLP portal, open the `dev-artifactory1` container page and **disable** the **production environment** in the **loadbalancer settings**.
* **Enable** the **standby environment** in the same loadbalancer settings.
* **Enable replication** by running the Jenkins job `dev-artifactory-create-replicationconfig`.

Both Jenkins jobs are aware of the current active Artifactory server.

{% include footer.md %}
