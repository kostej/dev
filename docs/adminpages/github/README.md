{% include admin-header.md %}

## Github administration and usage

### [Git team workflow](workflow.md)
Changing, committing and approving sourcecode is the most common git activity for the DevForce team. We need some rules how we do this.

### [Github outside collaborators](outside-collaborators.md)
Outside Collaborators are difficult to manage in Github. All Outside Collaborators and repo's are managed on this page.

{% include footer.md %}
