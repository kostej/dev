{% include admin-header.md %}

# Github Outside Collaborator administration
It is hard to manage Github Outside Collaborators because there is no way to communicate with them if necessary. Github does not provide administrator functions to accomplish this. Therefore, the *Product Owner of Development Services* has decided that some form of administration should be performed. This page is the implementation 
of that requirement. 

Because there's no automatic removal of Outside Collaborator accounts accessing these Github repositories, we will regularly check (monthly) whether these Outside Collaborators are still required to have access to the specified repositories. 

# Teams using Github Outside Collaborators

## Datascience team project xxxx
**Contact:** Rob Wenneker  
**Manager:** Charl Vermeer  
**Requested:** 05-06-2021

## History
**05-06-2021:** Added collaborators BrittBekkenutte, tomr-cgi, ArnoutCGI, Redmer  
**23-08-2021:** Removed collaborators tomr-cgi, ArnoutCGI, Redmer

### Github Outside Collaborators

| Number | Name               | Company email                | Github Account  | Comment    | 
| :----- | :----------------- | :--------------------------- | :-------------- | :--------- | 
| 1.     | Britt Bekkenutte   | britt.bekkenutte@cgi.com     | BrittBekkenutte | -          | 
| 2.     | ~~Tom Rozekrans~~      | tom.rozekrans@cgi.com        | tomr-cgi        | -          |
| 3.     | ~~Arnout van Dael~~    | arnout.van.dael@cgi.com      | ArnoutCGI       | -          |
| 4.     | ~~Redmer Kronemeijer~~ | redmer.kronemeijer@cgi.com   | Redmer          | -          |

### Concerned Github Repositories

| Github Repository                                                   | Outside collaborators | Access type |
| :------------------------------------------------------------------ | :--------: | :---: |
| https://github.com/kadaster-it/datascience-graphql-airflow          | All        | Write |
|	https://github.com/kadaster-it/datascience-graphql-bag              | All        | Write |
|	https://github.com/kadaster-it/datascience-graphql-bgt              | All        | Write |
|	https://github.com/kadaster-it/datascience-graphql-common           | All        | Write |
|	https://github.com/kadaster-it/datascience-graphql-enhancer         | All        | Write |
|	https://github.com/kadaster-it/datascience-graphql-open             | All        | Write |
|	https://github.com/kadaster-it/datascience-graphql-dkk              | All        | Write |
|	https://github.com/kadaster-it/datascience-graphql-brkpb            | All        | Write |



{% include footer.md %}
