{% include admin-header.md %}

# Git workflow

## Introduction

This page describes a base Git commit and pull request workflow to enforce a higher base quality of delivered code by letting a fellow developer review your changes. 

## Development workflow

### Prerequisites

- Git repositories have the configuration as described in the section **Repository branch configuration** below.
- Before code or documentation is modified, added or deleted, a **unique Jira issue** exists, *at any time*. If not, create a Jira issue first.

### Git workflow
**Code** and **documentation changes** flow from **feature branches**, to the **develop branch**, and end in the **master branch**. During these transitions a fellow developer will review the changes in a **Pull Request** and **approves them, rejects them or requests changes**. In case of approval you, as a developer, can continue with the next step in the workflow. Otherwise, you will have to improve your changes first and create a new Pull Request. 

### "Feature" branches
- Feature branches are used to resolve **only one Jira issue** and are used to check out the code to your development laptop.
- Feature branches must be named **feature/\<jira-issue-code\>-\<short-description\>**, e.g. **feature/SO-3914-git-workflow-docs**.
- Commits **always** have a **commit message**. Commit messages are **short** (max. 50 characters) and **meaningful** and should contain the **Jira issue code**, e.g "**SO-3914 Git workflow documentation added**". Do not place a colon after the Jira issue code (e.g. "**SO-3914: ..**") to save one character because commit messages always tend to be too long.
- After pushing changes to the remote Git repository a **Jenkins build must be triggered**, optionally followed by a **deployment in the test environment**.

### "Develop" branch
- The **develop** branch in a Git repository is used to integrate changes from a **feature branch**. This accommodates collaboration between developers working on the same component but working on different changes. Changes are only integrated after being tested and approved.
- Changes are integrated in the Develop branch with a **Pull Request** from the feature branch, but only **after approval** by one or more **team members**.
- A **pull request** is created with a **short description** that also contains the **Jira issue code**, e.g. **SO-3914 Git workflow documentation added**.
- After the pull request has been approved and the changes have been integrated in the **develop branch**, a **Jenkins build will be triggered** that creates a **snapshot version** of the component, followed by a **deployment in the test environment**. 

### "Master" branch
- After approval of one or more changes in the **develop branch**, the team may decide to **release these changes** to the **master branch**. 
- A developer prepares the **Pull Request** from **develop** to **master** and another team member approves or rejects it.
- After the pull request has been approved and the changes have been integrated in the **master branch**, a **Jenkins build will be triggered** that creates a **release version** of the component, followed by a **deployment in the test environment**. A last test can be performed here.

### Deployment in production
- Deployment in the production environment is **explicitly executed manually** by a developer. 

# Git repository branches configuration

To accommodate the workflow, **all team repositories** have the **same branch settings** and **branch protection** rules. These settings must be configured **directly after repository creation** (and are also enforced on all existing repositories):

## Configure the default branch

The **default branch** of a Git repository is always **develop**. Configure it as follows:

- Goto https://github.com/kadaster-it/your-repo-name.
- Click the **Branch selector** button (left from **\<NUMBER\> branch** option) in the menu bar
- If the list of branches button does not contain **develop**, type **develop** in the box containing **Find or create a branch...** and type **Enter**. A branch named **develop** is created now.
- Click the **Settings** tab in the menu bar.
- Click **Branches** in the left menu.
- Select **develop** from the drop-down button in the **Default branch** section and click **Update**.
- A warning message appears. Accept it by clicking **I understand, update the default branch**.

## Configure the branch protection rules 

### "develop" branch protection rule

Add a **branch protection** or check the rule for the **develop** branch as follows:
- Goto https://github.com/kadaster-it/your-repo-name 
- Click the **Settings** tab.
- Click **Branches** in the left menu.
- Click the **Add rule** button right from the **Branch protection rules** section header. Type **develop** in the **Branch name pattern** field. Click **Edit** if the rule already exists. 

Check whether the following settings are configured:

- Select **Require pull request reviews before merging**.
- Set **Required approving reviews** to **1**.
- Select **Dismiss stale pull request approvals when new commits are pushed**. 
- Select **Include administrators**. 
- Click **Save** or **Save changes** at the end of the page.

### "master" branch protection rule

For the **master branch protection** execute the same steps as described for the **develop** branch. There's one exception, the **Branch name pattern** field must be **master**.

# Exceptions to the workflow

## The workflow feels too heavy for some repositories
The workflow as described on this page is the **default workflow**. Sometimes, and only when a second team member approves the decision, this workflow feels a bit heavy, e.g. for a **documentation repository**. In that case:
- Do not specify the **develop** branch protection rule.
- You are allowed to **push** to the **develop branch** directly in that case.
- You are still required to create a pull request to integrate the changes into the master branch. A team member must still approve the (documentation) changes before they are published.

## You have to release changes to production and no reviewer is available
Sometimes you have to solve an incident but no reviewer is available to approve your Pull Request. In that case you can **temporarily disable the branch protection rules** as a workaround. Everyone in the team has **repository admin permissions** so this should be no problem. 

But remember:
- **Do not forget to enable the branch protection rule(s) afterwards**.
- **Do not abuse this workaround**.

{% include footer.md %}

