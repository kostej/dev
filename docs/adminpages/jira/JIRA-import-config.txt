{
  "config.version" : "2.0",
  "config.project.from.csv" : "true",
  "config.encoding" : "UTF-8",
  "config.email.suffix" : "@",
  "config.field.mappings" : {
    "Project name" : {
      "jira.field" : "project.name"
    },
    "Description" : {
      "jira.field" : "description"
    },
    "Project url" : {
      "jira.field" : "project.url"
    },
    "Custom field (Epic Link)" : {
      "existing.custom.field" : "10011"
    },
    "Created" : {
      "jira.field" : "created"
    },
    "Outward issue link (Relates)" : {
      "link.type" : "10020"
    },
    "Custom field (Flagged)" : {
      "existing.custom.field" : "10016"
    },
    "Project type" : {
      "jira.field" : "project.type"
    },
    "Votes" : {
      "jira.field" : "votes"
    },
    "Resolved" : {
      "jira.field" : "resolutiondate"
    },
    "Time Spent" : {
      "jira.field" : "timespent"
    },
    "Remaining Estimate" : {
      "jira.field" : "timeestimate"
    },
    "Issue id" : {
      "jira.field" : "issue-id"
    },
    "Status" : {
      "jira.field" : "status"
    },
    "Custom field (Team)" : {
      "existing.custom.field" : "10810"
    },
    "Assignee" : {
      "jira.field" : "assignee"
    },
    "Comment" : {
      "jira.field" : "comment"
    },
    "Updated" : {
      "jira.field" : "updated"
    },
    "Custom field (Start date)" : {
      "existing.custom.field" : "10511"
    },
    "Issue key" : {
      "jira.field" : "issuekey"
    },
    "Priority" : {
      "jira.field" : "priority"
    },
    "Parent id" : {
      "jira.field" : "subtask-parent-id"
    },
    "Reporter" : {
      "jira.field" : "reporter"
    },
    "Labels" : {
      "jira.field" : "labels"
    },
    "Due date" : {
      "jira.field" : "duedate"
    },
    "Fix versions" : {
      "jira.field" : "fixVersions"
    },
    "Outward issue link (Cloners)" : {
      "link.type" : "10030"
    },
    "Custom field (Rank)" : {
      "existing.custom.field" : "10110"
    },
    "Custom field (Target end)" : {
      "existing.custom.field" : "10813"
    },
    "Issue Type" : {
      "jira.field" : "issuetype"
    },
    "Project lead" : {
      "jira.field" : "project.lead"
    },
    "Project key" : {
      "jira.field" : "project.key"
    },
    "Custom field (Target start)" : {
      "existing.custom.field" : "10812"
    },
    "Environment" : {
      "jira.field" : "environment"
    },
    "Summary" : {
      "jira.field" : "summary"
    },
    "Custom field (Story Points)" : {
      "existing.custom.field" : "10018"
    }
  },
  "config.value.mappings" : { },
  "config.delimiter" : ",",
  "config.project" : {
    "project.type" : null,
    "project.key" : "",
    "project.description" : null,
    "project.url" : null,
    "project.name" : "",
    "project.lead" : null
  },
  "config.date.format" : "dd/MMM/yy hh:mm aa"
}