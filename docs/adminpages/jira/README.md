{% include admin-header.md %}

## Jira administration and usage

### [Jira upgrade](jira-upgrade.md)
The most common Jira lifecycle management activity is a Jira upgrade.

### [Database migration](database-migration.md)
Sometimes Jira's database must be migrated to another database container.

### [Storage container migration](storage-migration.md)

### [Import/export issues](issue-migration.md)
Issues can be moved from one instance to another. Things described here can make such a migration easier.

{% include footer.md %}
