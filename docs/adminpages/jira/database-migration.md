{% include admin-header.md %}

## Jira database migration

Sometimes it is necessary to migrate the Jira database to another database instance. E.g. when PLP offers a new type database container with a new version of Postgres. In that case follow the steps that are described on the page to migrate Jira.

### Assumptions
1. The **Jira Postgres database** will be migrated to a database on a **new database container**.
1. The **Jira NFS storage** remains on the **same NFS storage container**.
1. The **Jira database** is created **manually** using the tool **PGADMIN4**.

### Steps to migrate 

#### Step 1 - Configure a new database on a new host
This step can be performed days before the actual migration of Jira.
1. **Activate the Kadaster VPN** to connect your laptop to the **internal Kadaster network**.
1. Using the **PLP portal**, request a **new PDB Postgres database container**, **single instance**. Check the sizing of the current Postgres database container and use these settings for the request.
1. Using PGADMIN4, create a **database server connection**:  
   - Right-click in the left pane **Servers -> Create -> Server....**
   - Tab General - Name = **\<computename\>.\<environment\>.kadaster.nl** (NOT the container's VIP address)
   - Tab Connection - Host = **\<computename\>.\<environment\>.kadaster.nl**
   - Tab Connection - Port = **49187**
   - Tab Connection - Maintenance database = **postgres**
   - Tab Connection - Username = **postgres**
   - Tab Connection - Password = **(see EBB/pg-user/postgres_credentials.txt)**.
   - Click **Save**
1. Using PGADMIN4, create the **Jira database user**:
   - Right-click **Login/group roles -> Create -> Login/group role ....**
   - Tab General - Name = **jira**
   - Tab General - Comment = **Jira database user**
   - Tab Definition - Password = **(see DevForce keypass Database)**
   - Tab Privileges - Can login ? = **Yes**
   - Tab Privileges - Inherit rights from the parent roles ? = **Yes**
   - Click **Save**
1. Using PGADMIN4, create the **Create the Jira database**:
   - Right-click: **Databases -> Create -> Database ....**
   - Tab General - Database = **jiradb**
   - Tab General - Owner = **jira**
   - Tab General - Comment = **Jira database**
   - Tab Definition - Encoding = **UTF8**
   - Tab Definition - Template = **template0**
   - Tab Definition - Tablespace = **pg_default**
   - Tab Definition - Collation = **C**
   - Tab Definition - Chartype = **C**
   - Tab Definition - Encoding = **UTF8**
   - Tab Definition - Connection limit = **-1**
   - Click **Save**

#### Step 2 - Publish a maintenance message
Do this **some hours before the actual migration**.
1. A **maintenance message** for Jira Users can be published using Jira's **announcement banner**.
1. Log into the Jira instance and go to the **Jira Administration -> System** page.
1. Click in the left menu **Announcement banner** under **User interface**.
1. Type an **announcement message**. 
1. Click **Set banner**.

#### Step 3 - Create a Jira backup
The actual migration starts by creating a backup file. The backup contains the Jira data that will be migrated to the new database instance.  
1. As a **Jira System Administrator** sign in to the Jira instance to migrate.  
**WARNING**: use the internal path [https:///dev-svc.\<in|cs\>.kadaster.nl]() otherwise the backup creation will timeout and fail !
1. **Change te anouncement banner** and publish a message that **all modifications will be lost from now on**.
1. Click in the upper right corner **Jira Administration -> System**
1. Click in the left menu **Backup system** under **Import and export**
1. To **Backup Jira data** type the file name **backup.zip** and click **Backup**. Do not use another filename otherwise the backup cannot be imported in a later step.
1. If the file already exists, click **Replace file**. If not, skip this step.
1. The backup has succeeded when the message **Data exported to: /var/atlassian/application-data/jira/export/backup.zip** is displayed.

#### Step 4 - Stop Jira
Bring Jira down by setting the **scale factor** of the **Jira Docker service** from **1 -\> 0** at its configuration page.
1. Login to the **UCP Cluster Manager** in which Jira runs.
1. Click **Services** in the left menu under **Swarm**.
1. Click **jira_jira**.
1. In the upper right corner click **Configure (the Cog-wheel)**
1. Click **Scheduling** in the left menu.
1. Set **Scale** to **0** and click **Save**. The swarm service overview will be displayed again.
1. Wait until Jira has stopped. The status of the **jira_jira** service will become **red and 0/0** after about 15 seconds (press F5 repeatedly in your browser).

#### Step 5 - Switch the Jira DNS alias 
A DNS alias is used to point to the Jira database container. Switch it to the new database container.
1. Using the **PLP Portal**, **remove** the **DNS alias dev-jiradb** from the **old Postgres Database Container**.
1. **Add** the **DNS alias dev-jiradb** to the **new Postgres Database Container**.
1. Check whether the DNS alias **points to the proper database instance** using **nslookup**.

#### Step 6 - Start Jira again
Start Jira by setting the **scale factor** of the **Jira Docker service** from **0 -\> 1** at its configuration page.
1. Login to the **UCP Cluster Manager** in which Jira runs.
1. Click **Services** in the left menu under **Swarm**.
1. Click **jira_jira**.
1. In the upper right corner click **Configure (the Cog-wheel)**
1. Click **Scheduling** in the left menu.
1. Set **Scale** to **1** and click **Save**. The swarm service overview will be displayed again.
1. Wait until Jira has started. The status of the **jira_jira** service will become **green and 1/1** (press F5 repeatedly in your browser).

#### Step 7 - Restore the Jira backup
1. The existing backup can be restored when [https:///dev-svc.\<in|cs\>.kadaster.nl/jira]() the page **Set up application properties**.
1. Click **import your data**. The page **Import existing data** is displayed.
1. Type the **File name** which must be **backup.zip** and select **Disable Outgoing mail**.
1. Click button **Import** to start restoring the backup. The page **Import existing data** is displayed.
1. The import and re-indexation takes about 1 hour.

#### Step 8 - Enable Jira outgoing email again
Now you can enable the **Jira outgoing email** again.
1. Log into the Jira instance and go to the **Jira Administration -> System** page.
1. Click in the left menu **Outgoing mail** under **Mail**. 
1. Click the button **Enable outgoing mail** at the upper right corner of the page.

#### Step 9 - Test, test, test, ...

{% include footer.md %}
