{% include admin-header.md %}

## How 2 migrate issues from Cloud to Server
Jira issues can be moved from a Cloud instance to a Server instance.
To do this follow the steps described below.

### Create a target Jira project
Create a project in the target instance with the same *Project Name* and *Project Key* as the source instance. If this is not possible add a find and replace for the *Project Name* and *Project Key* in the `createImport.sh`, mentioned in the next step.
Make sure issues in the Project's Workflow are editable in a *Status Closed* (`jira.issue.editable=true`), otherwise attachments cannot be added to the issues in this state.
The person that will eventually run the script to add the attachments again must have *Developer* permission in the target project.

### Create a script `createImport.sh` to convert entities
The Jira import will fail completely if it runs into an unknown user as *Reporter* or *Assignee*. It will simply ignore an unknown user as *Watcher*.

Importing an issue will fail if the *Type* or *Status* is unknown.

You can create an issue export for test purposes, import it into Excel and manually compare values users, issue types and status in source and target instance.

Determine a valid mapping of these values and create a script to find and replace them in a given export CSV-file. For users replace both usernames and uuid's with the userid valid in the target instance.

This is an example with `sed` in `bash`:
```
#!/bin/bash

# --- createImport.sh ---

# Copy the source to a file with fixed name
cp -f "$1" JIRA-import.csv

# Replace userid's
sed -i \
-e "s/Jan.Klaassen/klaasj/g" -e "s/557058:d393651b-7062-4e95-aa40-2389d099172e/klaasj/g" \
-e "s/Cali.Mero/meroc/g" -e "s/ug:06de9a73-15aa-499b-acaf-9f003323c55a/meroc/g" -e "s/5fff16354d2179006e06392f/meroc/g" \
JIRA-import.csv

# Replace issue statusses
sed -i \
-e "s/Interne review/Reviewing/g" \
-e "s/Test/Verifying/g" \
JIRA-import.csv

# Replace issue types
sed -i \
-e "s/Feature/New Feature/g" \
JIRA-import.csv
```

### Freeze the source instance
Make the Jira project containing the source issues *Read Only*. For example. create a *Permission Scheme* with only *Browse* permissions for Users and *Administer* permissions for Admins and switch to that scheme.

### Export Attachments
File Attachments need to be exported/imported manually:
- Create a JIRA server backup using the *System* menu 
- Extract the directory with attachments from the backup file
- Order the attachments in a directory structure like this: ```<project key>/<issue key>/<attachement file(s)>```

The directory structure with attachments doesn't contain attachments with their original filename. We need to create and run a script to rename them:
- Copy the `<FileAttachment/>` lines from the `entities.xml` in the backup file to a script file and transform them into directory rename statements. For example using shell:
  ```<FileAttachment id="10000" issue="10000" mimetype="image/jpeg" filename="System.jpg" created="2016-10-21 06:21:45.19" filesize="79939" author="Cali.Mero" thumbnailable="1"/>``` must be transformed into ```find -type f -name "<id>" -execdir mv {} "./<filename>" \;```

### Export issues
- Export the issues of type *Epic* (fileName `JIRA.csv`), using *Issue Search* and *Export to CSV*.
- Export the first 1000 (or less) issues of type other than *Epic* (ORDER BY key ASC, filename `JIRA (1).csv`)
- The next 1000 issues (or less) issues of type other than *Epic* can only be exported by using an offset. For this copy the URL of *Export to CSV* to the address bar in a new browser tab and add the offset as query argument: `&pager/start=1000` (fileNames: `JIRA (<number>).csv`).
- Repeat the former step and increase the offset for each export with `1000` until all issues are exported (the exported file has a size smaller than all the others or `size=0`).
  
### Import issues
For each export file:
- Run the `creatImport.sh <export file>` that replaces *Issue type*, *Status* and *User(uuid)*
- Start an External System Import in the target JIRA.
- Select import file `JIRA-import.csv`
- Select config file [`JIRA-import-config.txt`](JIRA-import-config.txt)
- Click `Next`, `Next` and `Begin import`
- Download and save the log-file with the base name of each `<export file`
  
### Add attachments
To add the attachments to the imported issues, store and run script [`importAttachments.sh`](importAttachments.sh) in attachment subdirectory `<project key>`.

