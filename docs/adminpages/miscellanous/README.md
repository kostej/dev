{% include admin-header.md %}

## Miscellaneous administration and usage

### Monitoring (*Planned, only partial operational!*)
Monitoring is based on Prometheus metrics and Grafana visualization:
![Monitoring setp](../../../docs/images/monitoring.png)

Prometheus scrape endpoints are configured in alle application services.
There is a Prometheus instance in the Dev cluster that scrapes these endpoints.
Alerts are defined using Alert Manager.
Slack is used as a Notification Service.
Grafana is used to draw charts using Prometheus as a Dataset.

**N.B.: Monitoring is not fully implemented yet!**

{% include footer.md %}
