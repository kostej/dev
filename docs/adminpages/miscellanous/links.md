{% include header.md %}

# Service maintenance URLs
## Production environment

| Component | Description | VPN required ? | Go there | Required privileges | 
|:---|:---|:---|:---|:---|
| UCP Cluster | Cluster administration | Yes | [:arrow_forward:](https://dev-ucp.cs.kadaster.nl:8443) | Group membership ? |
| UCP Cluster | Traefik Administration | Yes | [:arrow_forward:](http://dev-svc.cs.kadaster.nl:8080) |
| Github | Enterprise administration | No | [:arrow_forward:](https:/github.com/enterprises/kadaster) | You must be enterprise owner |
| Github | Organization administration | No | [:arrow_forward:](https:/github.com/enterprises/kadaster) | You must be organization owner |
| Jira | Administration | No | [:arrow_forward:](https://dev.kadaster.nl/jira) |  |
| Sonar | Administration | No | [:arrow_forward:](https://dev.kadaster.nl/sonar) |
|  |  | [:arrow_forward:]() |
|  |  | [:arrow_forward:]() |
|  |  | [:arrow_forward:]() |
|  |  | [:arrow_forward:]() |
|  |  | [:arrow_forward:]() |
|  |  | [:arrow_forward:]() |
|  |  | [:arrow_forward:]() |
|  |  | [:arrow_forward:]() |
|  |  | [:arrow_forward:]() |
|  |  | [:arrow_forward:]() |
|  |  | [:arrow_forward:]() |
|  |  | [:arrow_forward:]() |

## Test environment



{% include footer.md %}
