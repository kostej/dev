[plantuml]
----
!includeurl ../../../libraries/Archimate.puml
Grouping(GGH,"Dev Cluster") {
  Application_Component(DPR,"Prometheus")
  Application_Component(DAM,"Alert Manager")
  Application_Component(DGR,"Grafana")
  Application_Component(DGT,"Gitea")
  Application_Component(DJM,"Jenkins")
  Application_Component(DAF,"Artifactory")
  Application_Component(DJI,"Jira")
  Application_Component(DSQ,"SonarQube")

  Rel_Serving(DJI, DPR, "metrics")
  Rel_Serving(DAF, DPR, "metrics")
  Rel_Serving(DSQ, DPR, "metrics")
  Rel_Serving(DGT, DPR, "metrics")
  Rel_Serving(DJM, DPR, "metrics")
  Rel_Serving(DPR, DAM)
  Rel_Serving(DPR, DGR, "dataset")
}

Application_Service(SLA, "Slack")
Rel_Serving(DAM, SLA, "Notifications")
----


