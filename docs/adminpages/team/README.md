{% include admin-header.md %}

## DevForce team 

### [Onboarding](onboarding.md)
Learn how to onboard the DevForce team faster by following the steps mentioned on this page.

### [Focus Area Groups](focus-area-groups.md)
The DevForce Team is divided into a number of Focus Area Groups. These groups are responsible for refining a certain area of the Jira backlog.

### [Refinement Process](refinement.md)
The Focus Area Groups organize a refinement session each fortnight. A Jira board supports this session.

### [Priviliged Access Management (PAM)](pam.md)
Kadaster has implemented PAM to gain elevated permissions.

### [Holiday handover](handover.md)
During holidays administration is handed over to other team members. Some practical information can be useful.

{% include footer.md %}
