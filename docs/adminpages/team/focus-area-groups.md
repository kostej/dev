{% include admin-header.md %}

## Focus Area Groups
The following **Focus Area Groups (FAG)** have been recognized within the DevForce team:

| Focus Area Group     | Members  |
|:---------------------|:---------|
| Development Services | Hugo Hoitink, Bert Meedendorp, Kevin Hensel, Hans Koster |
| Platform Services    | Thijs Cramer, Luuk Tersmette, Harold-Jan Vuyk, Kevin Hensel, Bert Meedendorp |
| Selfservice          | Richard Beumer, Hugo Hoitink, Luuk Tersmette |
| Kadaster IT Monitoring & Tactische Beheer Organisatie | Harold-Jan Vuyk, Richard Beumer |
| Architectuur         | Hugo Hoitink, Thijs Cramer |
| Team                 | Hugo Hoitink, Nico Pranger, Hans Koster |

## Responsibilities
- Each FAG is responsible for refining the Jira issues that appear in the `product backlog` and the columns of the 
[DevForce Refinement Board](https://dev.kadaster.nl/jira/secure/RapidBoard.jspa?rapidView=1192&useStoredSettings=true)
- Each FAG is self-organizing, which means that refinement sessions are organized regularly by the individual groups.
- Each FAG takes care of ranking and refining enough Jira issues for the `sprint backlog` (which only contains Jira issues with the status `Ready for Sprint`. The sprint backlog should contain issues to fill about 2 sprints.

{% include footer.md %}
