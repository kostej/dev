{% include admin-header.md %}

{% include toc.md %}

## Holiday handover (Hans Koster)
Items decorated with a speech balloon ( :speech_balloon: ) deserve attention and possibly some action. 

## General remarks
### Service requests from clients
Service requests issued by clients are received through the following channels (which should be checked regularly):
- Team mailbox soo@kadaster.nl  
  - Note: Do not throw away any mail (except recurring mails from ServiceNow) !   
  - Assign a mail to the person who is going to handle the mail (mostly you ;-) ) in the  **Category** column
  - Assign a symbol in the **flagstatus** column immediately. Assign a red flag (the mail line becomes yellow) when the request is being handled or is waiting to be handled. Assign the checkmark symbol when the request has been handled successfully. This informs other teammembers reading the mailbox.
  - Always reply to ALL (including soo@kadaster.nl) so anyone can see how the request was handled if they are interested. Do not assume that every client knows backgrounds in your reply.
- Slack
  - The following channels are watched #dev, #github-cloud-migratie, #sonar, #jetbrains.
  - Always reply in a thread. This makes it easier to follow the communication concerning this single request.
- Servicenow
  - ServiceNow incidents are not received very often. 
  - A mail is received in the mailbox soo@kadaster.nl when incidents are assigned to DevForce.
  - These incidents mostly concern creating accounts for Jira, Github, Slack. Alerts concerning PLP machines are also received there.
  - Often people don't know that requests are selfservice. Stay calm and reply nicely.

### :speech_balloon: Corneel van Holland
For our new collegue Corneel van Holland (who starts 1-7) some Development tools accounts must be created. I have asked Ronald Dekker to request a **Werkplek 2.0 account** which is required to create the following accounts (please create these account as soon as possible):
  - Github
  - Jira
  - Slack
  - Splunk
  - Etc.
  - Also assign PLP roles for (DEV, PLS, KIM, TBO, SUSE)

### Status github.so.kadaster.nl to github.com migration 
- All NON-PDOK accounts (except ZZ_Jenkins-xxx accounts and kloppr2 and hebbim) of github.so.kadaster.nl have been suspended.
- PDOK is busy migrating. Please ask Marcel Brands regularly whether migration is progressing.
- After 27-6-2021 access to NON-PDOK repositories is provided by delivering a `zipped BARE repository clone` to the requestor. Stick to this please, because access to github.so.kadaster.nl will not be granted anymore. This has been announced in Slack #dev-announcements ! 
- When PDOK is ready migrating, all repositories will be archived to the organisation https://github.com/kadaster-archive (see https://dev.kadaster.nl/jira/browse/SO-4286). No action required right now. This will be done after the summerholidays.

### Locked accounts
Some people complain about locked accounts. This not always the case. See the Jira section on this page.
- Check this using https://gaa.kadaster.nl/user/\<accountname\>.
- This URL can also be used to check membership of AD groups like `ontwikkelaars` and PLP groups.

## Jira
### Account requests
Jira account requests are received regularly. Process:
- Try to guess the Windows Active Directy account.
- Use https://gaa.kadaster.nl/user/\<accountname\> to check the accountname. Sometimes the accountname cannot be guessed. Use the Outlook addressbook in that case. Copy the email adress.
- Create the Jira account using: https://dev.kadaster.nl/jira/secure/admin/user/UserBrowser.jspa. Click button **Create user**.
- Specify **Email address**, **Full Name**, **Windows Active Directory accountname**
- Remove the dots in the **Password** fields.
- Accept everything else (do not send mail) and click **Create User**.

Sometimes it is difficult to guess the accountname. Outlook Addressbook does not help for new accounts (the alias field does not contain the Windows Active Directory account anymore. Alias email addresses are also not created anymore). Guessing account fails when people have Christian first names or when someone else possesses the account. A 1 or 2 may have been added to the account. As a last resort: ask the requestor.

After creating the account a mail is send to the requestor containing the following text:

>\<Naam\> kan nu met het account \<accountnaam\> inloggen in Jira via https://dev.kadaster.nl/jira en het wachtwoord van Werkplek 2.0. Het account heeft de volgende basisrechten omdat Jira samenwerking tussen teams ondersteunt en stimuleert:
> - In principe mogen Jira gebruikers overal kijken (het account heeft dus leesrechten).
> - Gebruikers mogen binnen elk Jira Project Jira issues aanmaken (meldingen doen voor het project).
> - Gebruikers mogen aan elk Jira Issue commentaar toevoegen.
>
>Het team waarbinnen \<naam\> gaat werken zal zelf de rol **Developer** aan het account moeten geven binnen het betreffende **Jira Project**. De **Jira Project Administrator** van het Jira Project kan dit doen. Dat is **team selfservice**.

### People cannot login into Jira
Sometimes people cannot login into Jira anymore. 
- Check whether their Jira account is active (Jira usermanagement-\>Edit account). If not, make it active. Send a mail reply to the user specifying that the account is active now and that they can login.
- If the account was active, send the user the following URL because they have to fix it themselves: https://dev.kadaster.nl/jira/secure/Dashboard.jspa. Explain the selfservice in the reply mail.

## People cannot login to Splunk or Jenkins
These tools are also connected to Windows Active Directory. When people change their password in Azure Active Directory (Werkplek 2.0), synchronisation with Windows Active Directory may fail. See **People cannot login into Jira** on this page for the selfservice workaround.

## Jenkins roles \*-ont-dep
Sometimes people request to create a Jenkins account because they cannot change Jenkins jobs. Reply that they should be assigned the proper **\*-ont-dep** role. This is team selfservice. A team member with the role **\*-ont-adm** can assign this role.

## Github
Kadaster Gihub organization have different properties. Yes, we are not happy with this :-( ).
### kadaster-it organization
#### Creating accounts or adding an account to a github-team
- Is the new account requested by someone from the **github-team-xxx** ? People are not allowed to request rights for themselves for security reasons, therefore the request should be issued by someone else from their team. Use the script `.\script_memberGithub.ps1` (see https://github.com/kadaster-it/dev-github-beheer/blob/develop/script_memberGithub.ps1) to check this. You need AzureAD user admin rights to use this script ! 
- Check whether the requested person has a Kadaster email adress. Check this using Outlook Addressbook. Copy the email adress.
- Add the email address to the requested **github-team-xxx** using the script `.\script_memberGithub.ps1`. Also add the same email address to the group `users`.
- Invite the person using the page https://github.com/orgs/kadaster-it/people. Click **Invite member**.
- Provide the email address in the dialog. Select the email address and click **Invite**. By using the email address:
  - You don't need to know the Github accountname of the person who needs the Github account.
  - You don't send mail to a unknown email address.
- Select the **Member** role and click **Send invitation**.
- Send a reply email to the requestor.

Note:
Requests for Github accounts may also be issued in Slack channel #github-cloud-migratie.

#### Repositories
Organization members can create repositories themselves. 

#### :speech_balloon: Leonie van de Wiel
She, and others, have to deliver (schema and zip-)files to the repository https://github.com/kadaster-it/iv-schemas which must be published on the kadaster.nl website. To do this, they had a really complicated manual (that includes installation of git client software, PAT, accounts, branching, doing everything on the commandline, pushing, etc) which is not suitable for beginners. I have recommended them to use the Github Web GUI. Using the GUI they can:
- Create a branch.
- Upload/edit the 3 files.
- Commit these changes.
- And create a pull request.

The IV team merges these changes into develop and master branches and publishes them on kadaster.nl. This is much more simple for them. 

Leonie asked me to educate these users (I made an exception for her). I refused that because they can ask their team counterparts. Maybe she will still ask questions. 

#### Outside collaborators
Outside collaborators are allowed since this month. Administer specific info at https://github.com/kadaster-it/dev/blob/develop/docs/administration/github/outside-collaborators.md.

### kadaster organization
#### Accounts
In the `kadaster` Github organization there are no `github-team-xxx` groups. To make SSO with Azure AD work you need to add the email adress to `github-team-kadaster-org` (use `kadaster-org` in the script). Also add the email to `users`.
#### Outside collaborators
Outside collaborators are mostly trainees. You need to add them specifically to repositories.
#### Repositories
For historical reasons repositories cannot be created by organization members. DevForce creates these repositories.

#### :speech_balloon: Note
John Horn and Fuat Akdeniz (team KLIC) have requested to rename the repository https://github.com/kadaster/klic-win to https://github.com/kadaster/klic. However, they are very unsure about this because the repository uses Github issues. I have tested and shown this to John Horn. Just renaming the repository results in what they want. 
Currently, the repo https://github.com/kadaster/klic already exists but this repo must be deleted. So the following must be done:
- Discuss the following steps with John Horn explicitly (again) !
- **Delete** the current repository https://github.com/kadaster/klic (it contains the same files as https://github.com/kadaster/klic-win). Ask whether they are ok with this !
- **Rename** https://github.com/kadaster/klic-win to https://github.com/kadaster/klic. All existing files and Github issues will be available in the repo https://github.com/kadaster/klic.
- **Create** a new empty repository https://github.com/kadaster/klic-win. Klic members will add a file README.md in the root which redirects (outside) users to the new https://github.com/kadaster/klic repo. Disable **Issue creation** in the settings of the NEW https://github.com/kadaster/klic-win repo to prevent Issue creation in this repo.  

### PDOK organization
#### Accounts
They add accounts themselves. Sometimes this is requested. No Azure AD groups are needed.
#### Outside collaborators
None (except a build account `koala-pdok`).

### kadaster-labs
In the `kadaster-labs` Github organisation there are no `github-team-xxx` groups. To make SSO with Azure AD work you need to add the email adress to `github-team-kadaster-labs-org` (use `kadaster-labs-org` in the script). Also add the email to `users`.
Add the account to the group https://github.com/orgs/kadaster-labs/teams/sensrnet manually. 

## Altova Licenses
The Altova License Manager manages 20 Kadaster XMLSpy licenses. These licenses may be consumed by 20x10=200 XMLSpy installations. The license manager manager maintains a list containing (IP addres, account) tuples. This list grows quickly because a laptop receives a dynamic IP addres when it is (re-)booted. When the list exceeds 200 entries no licenses can be issued to new clients, even when licenses are free ! See also https://github.com/kadaster-it/dev-altova-licenseserver/wiki. 

:speech_balloon: Workaround: 
- Goto http://dev-licserver1.so.kadaster.nl:8088/clientmanage. 
- Click on any entry.
- Click on button **Unregister client and all products**.
- Do this for all entries (some are in use and cannot be unregistered !)
- I have done this for you on 22-06-2021, performing this once a month should be sufficient. Otherwise, an incident will be reported by XMLSpy users.

## Other license managers
Two other license managers run on the same SWAN as the Altova Licenseserver.
- See https://github.com/kadaster-it/dev-sysam-licenseserver/wiki (don't know whether this info is up-to-date)
- and https://github.com/kadaster-it/dev-sparx-keystore-service/wiki (don't know whether this info is up-to-date)

## KLIC-viewer requests
Tijmen Kroon (KLIC) sometimes needs help.

### Windows signing
- See https://github.com/kadaster-it/dev-safenet-auth-client/wiki

Ask:
- Richard Beumer (but anyone can do this)

### Apple Appstore and Google Playstore
KLIC-viewer signing and certificates are used by the KLIC team through the **Github Actions** builds. No help is needed. 

The KLIC team sometimes requests to add users to the KLIC viewer APP development environment for the Apple Appstore (iOS en MacOS) and Google Playstore (Android). 

See DevForce keypass for URLs and other info.

Ask:
- Richard Beumer
- Harold-Jan Vuyk
- Sjanette van Berlo (Account Holder, she is sometimes needed to Accept Agreements, klic cannot build in that case .....)

## Jetbrains
The assignment of Jetbrains licenses (All Products Pack (like IntelliJ) and Datagrip) is done using https://account.jetbrains.com/assets/all?customer=825801. 

Ask (they are also license Administrators):
- Hugo Hoitink
- Nico Pranger
- Marc van Andel

## Creation of INT and FTO accounts
The creation of INT and FTO accounts (or password resets) has been transferred to IAM (Ed van de Brink) when Henk Everts left Kadaster. These requests must be nicely routed to Ed (and inform the requester).

{% include footer.md %}
