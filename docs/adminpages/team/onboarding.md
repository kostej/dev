{% include admin-header.md %}

# Team onboarding

## Laptop

## Accounts and permissions

## Agile Scrum Process

### [Issue intake](intake.md)
Newly created Jira issues are daily examined and enhanced as input for the Product Backlog Refinement meetings.

### [Backlog Refinement](refinement.md)
The [Focus Area Groups](focus-area-groups.md) organize one or more Product Backlog Refinement meetings during each sprint. 

### [Sprint planning](sprint-planning.md)
To be specified.

### ["The sprint"](sprint.md)
To be specified.

### [Sprint review](sprint-review.md)
To be specified.

### [Retrospective](retrospective.md)
To be specified.

### [Jira Process support](jira-process-support.md)
To be specified.

{% include footer.md %}
