# Priviliged Access Management (PAM)
Kadaster uses **Priviliged Access Management (PAM)** to access resources which are accessible only with Administrator permissions. In the past these permissions were given to your account. This is error prone and not very secure in modern times. This is were PAM comes in. PAM is an extra level of security between you and secured resources. Secured resources are now accessible only through a PAM account by checking out a secret: 
- The PAM account has the permissions to access specific resources (like Azure Active Directory user management).
- The password of a PAM account changes automatically and often. You don't need to know the password.

Kadaster has implemented PAM using **Thycotic Secret Server**, a cloud service.

- Click [here for the latest documentation on Secret Server](https://docs.thycotic.com/ss) at Thycotics' website.
- Click [here for the Kadaster PAM wiki](https://wiki.kadaster.nl/wiki/index.php/PAM).


## Getting started

### Install the WPF browser extension
The browser extension **Thycotic Secret Server Web Password Filler (WPF)** helps accessing and using Secret Server managed secrets directly from your browser.

- Add the WPF browser extension for your preferred browser (Google Chrome, Microsoft Edge or Firefox) using the browsers' webstore. 
- Click **Add (extension)**.
- After the extension has been added successfully, a dialog may appear depending on the browser:
  - Firefox
    - :heavy_check_mark: Allow this extension to run in Private Windows.
    - Click **Okay**
  - Chrome     
    > *NOTE: The latest Chrome WPF extension (3.0.1) suffers from a bug and does not work correctly ! You must temporarily use another browser for Secret Server*. 
    - Ignore the dialog by dismissing it.
    - Click on the jigsaw puzzle piece in the upper right corner of the browser and activate the pushpin of the WPF extension otherwise you won't be able to configure it in later steps on this page.
  - Microsoft Edge
    - Ignore the dialog by dismissing it.

### Configure Incognito or Inprivate browsing
The WPF extension must be allowed to open **incognito** browser windows (a.k.a **inprivate** browser Windows). 
- Goto **extension management** of your browser
- Search for the extension **(Thycotic) Secret Server Password Filler**
  - Firefox
    - Click **"..." -> Manage**
    - :heavy_check_mark: Run in Private Windows: **Allow**
  - Chrome
    - Click **Details**
    - :heavy_check_mark: Allow in Incognito: **Enable**
  - Edge
    - Click **Details**
    - :heavy_check_mark: Allow Inprivate: **Enable**

### Configure the WPF browser extension
- Click the grey lock icon in the extension bar in the upper right corner of your browser.
- Fill in the fields (Chrome) or click tab **Configuration** (Edge and Firefox)
  - Secret Server URL: https://kadpamprod.secretservercloud.eu  
  - Domain: local
  - :white_check_mark: Use secret server to login (Chrome only)
  - Click **Save**  or **Continue**
- Click tab **Settings** (Edge and Firefox) or click the **Cog wheel -> Preferences** (Chrome)
  - :heavy_check_mark: Use secret server to login (Edge and Firefox only)
  - :x: Prompt to save credentials
  - :heavy_check_mark: Show (available) credentials in a popup (when available) 
  - :x: Hide (viewonly) folders (when not allowed to add secret) 
  - :heavy_check_mark: Enable auto populate 
  - :heavy_check_mark: Default position for Secret Popup 
  - :x: Exact match Secret URL 
  - Click **Save** (Edge and Firefox) or click **\< Back** twice (Chrome)
- Click tab **Login** (Edge and Firefox) or click **Continue** (Chrome)  
  It's possible that the Username and Password fields are not shown. In that case your browser cached **Werkplek 2.0 credentials** are used.
  - Username: **your Kadaster email address**
  - Password: **your Werkplek 2.0 password**
  - :heavy_check_mark: Use secret server to login (Edge only)
  - Click **Login**.  
- The lock icon of the extension in the upper right corner of your browser should **turn green** after successful **authentication with Secret Server**.

You should be able to use **Secret Server secrets** now for Priviliged Access Management. Read on.

## Using Secret Server secrets
Depending on your role within the Kadaster organisation you have access to zero or more Secret Server secrets. Secrets are enabled for you by team **Identity and Access Management (IAM)**. 

### Checking out a secret
Before you can administer priviliged resources you have to checkout the secret that gives you access.
- Goto the [Kadaster Secret Server cloud application](https://kadpamprod.secretservercloud.eu) using your preferred browser.
- You should be logged into Secret Server automatically using your **Werkplek 2.0 account**, otherwise you know what to do to login.
- You can find secrets by clicking **Secrets** in the **left menu**. 
- **Navigate to the secret** you need for your specific administrative activity. 
- Click the name of the **secret**. It is possible that someone else has checked out the secret. In that case you have to wait because it must be **checkout in first** by its current user (either manually (ask the current user) or automatically (which may take a long time)).
- Click **Enter comment** and specify the **reason** why you need to checkout the secret to access the resource. Be specific and precise. The comment will be logged for audit trail purposes.
- Click **Checkout secret**. The attributes page of the secret appears.
- Click **Web Password Filler** below **Launchers** to **access the Priviliged resource** and to activate the required role for your administrative activity. An **Incognito window** is launched.
- **Check** whether you are logged in with an account that has **pam** in its name and that the browser window is an **incognito window**. If you are logged in with your own credentials something went wrong with the Web Password Filler browser extension.
- You can now perform the administrative activity.
- Don't forget to logout and close the browser window afterwards.
- Checkin the secret. Read on.

### Checking in a secret
After using a secret, you have the free it by checking it in. Don't forget this otherwise it cannot be used by others.
- Goto the [Kadaster Secret Server cloud application](https://kadpamprod.secretservercloud.eu) using your preferred browser.
- Login and go to the attributes page of the secret as described earlier on this page.
- Click the button **Check in** in the upper right corner of the page.
- Press **F5** multiple times when the **Checked Out** column still states **Yes**. It should state **No**.
