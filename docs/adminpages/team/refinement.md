{% include admin-header.md %}

## Backlog refinement meeting
Most of the time, the backlog contains many Jira issues not ready to be implemented. These issues must be refined. 

More info on backlog refinement:
- [Product Backlog Refinement explained (part 1 of 3)](https://www.scrum.org/resources/blog/product-backlog-refinement-explained-13)
- [Product Backlog Refinement explained (part 2 of 3)](https://www.scrum.org/resources/blog/product-backlog-refinement-explained-23)
- [Product Backlog Refinement explained (part 3 of 3)](https://www.scrum.org/resources/blog/product-backlog-refinement-explained-33)

### DevForce Refinement board 
To support the refinement meeting the Kanban Board 
[DevForce Refinement Board](https://dev.kadaster.nl/jira/secure/RapidBoard.jspa?rapidView=1192&useStoredSettings=true) has been created. 
The board supports the refinement meeting of all [Focus Area Groups](focus-area-groups.md). 

#### Backlog view
The backlog view can be used as follows:
- Open the [DevForce Refinement Board](https://dev.kadaster.nl/jira/secure/RapidBoard.jspa?rapidView=1192&useStoredSettings=true).
- Click `Backlog` on the left side of the board. The complete backlog of the DevForce team appears. This backlog contains issues with status `Open` and all other possible issues statuses that are not used on other DevForce boards. 
- Filter the backlog using one of the `Quick filters` at the top of the board that represents the `Focus Area` that will be subject of the refinement session.
- There's a quick filter for `Customer` issues if that is desired. 
- And quick filters for recently created issues or issued created long ago.

The backlog supports the following use-cases during the refinement meeting:
- Which issues have been created since the last refinement ? Use quick filter `Created last 15 days `. Determine whether issues need immediate attention. Use the quick filter `Customer` for further filtering.
- Which issues have been created somewhat longer ago and did not get attention yet ? Use one of the other time and period related quick filters. Ask yourself whether these issues will ever be implemented.  
- Ranking of issues.
- Have a quick look on issues to enhance their description and attributes. Click on an issue line and issue details appear at the right side of your screen.
- Drag some issues to the `Ready for refinement` area if you choose to refine (a number of) issues in that refinement meeting. They will subsequently appear in the left column of the Kanban board.
- Other use-cases may be invented of course.

#### Kanban board view
The Kanban board view supports the refinement process flow of individual Jira issues. The goal of refining Jira issues is to prepare issues for the `Ready for Sprint` backlog which is used during the `Sprint planning` meeting.

The Kanban board view can be used as follows:
- Open the [DevForce Refinement Board](https://dev.kadaster.nl/jira/secure/RapidBoard.jspa?rapidView=1192&useStoredSettings=true).
- Click `Kanban board` on the left side of the board. A column view of selected issues appears.
- Use the quick filters at the top of the Kanban board to narrow the view of the cards.

During the refinement process issue can be in one of following issue statuses (a card may be clicked in any column to display issue detail on the right of the screen):
- `Ready for refinement`  
An issue card will appear in this column after being selected and dragged to the `Ready for refinement` area at the board's `Backlog view`.
- `Analyzing`  
When the Focus Area Group members start refining an issue, it should be dragged into this column. 
- `Ready for poker`  
After refining an issue sufficiently, it should be dragged into this column and to start pokering for the number of Story points. This column can also be used as a waiting column if one wishes to wait before putting it in the sprint backlog.
- `Ready for sprint`  
If the Focus Area Group members wish to implement the Jira issue during one the coming sprint it should be dragged into this column. It adds the issue to the `Ready for Sprint` backlog on the DevForce Sprint Board.

{% include footer.md %}




