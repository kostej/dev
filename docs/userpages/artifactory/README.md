{% include header.md %}

## [Artifactory](https://dev.kadaster.nl/artifactory/)

Artifactory stores both Kadaster artifacts and artifacts downloaded from public repositories on the internet.

It supports storing the following types of artifacts:
- [Bower](#Bower) 
- [Chocolatey](#Chocolatey)
- [Docker](#Docker)
- [Gradle](#Gradle)
- [Maven](#Maven)
- [Npm](#Npm)
- [NuGet](#NuGet)
- [Python](#Python)
- [Unix](#Unix)

For each type of artifact there are typically the following repositories:
- A repository with `snapshot` in its name, suitable for storing Snapshot packages for Test environments
- A repository with `release` in its name, suitable for storing Release packages for Prod environments 
- A repository with `remote` in its name, suitable for storing/caching packages from a public registry 
- A virtual repository with `registry` or `virtual` in its name, exposing a transparent layer on all other repositories. This can and should be used for downloading packages, without having to worry if it is a Kadaster or public package

Apart from Docker, all repositories can be accessed through URL https://dev-brm.cs.kadaster.nl/<repository name>

### Bower
- `bower-snapshot-local`: Jenkins and members of `ontwikkelaars` have permissions to upload
- `bower-release-local`: Jenkins has permissions to upload
- `bower-remote` (remote is https://github.com)
- `bower-registry`

### Chocolatey
- `chocolatey-snapshot-local`
- `chocolatey-release-local`
- `chocolatey-org-remote` (remote is  https://chocolatey.org)
- `chocolatey-registry`

### Docker
- `docker-dev-local` (`https://dev-brm.cs.kadaster.nl:5001`): Jenkins and members of `ontwikkelaars` have permissions to push
- `docker-prod-local` (`https://dev-brm.cs.kadaster.nl:5002`): members of `<customer>-ont-dep` have permissions to push to tag `<customer>/*`
- `docker-remote` (remote is https://registry-1.docker.io)
- `docker-virtual` (`https://dev-brm.cs.kadaster.nl:5003`)
   
### Gradle
- `gradle-snapshot-local`
- `gradle-release-local`
- `gradle-plugin-remote` (remote is  https://plugins.gradle.org/m2)
- `gradle-registry`

### Maven
- `mvn-snapshot-local`: members of `<customer>-ont-dep` are allowed to upload to groupId `nl/kadaster/<customer>`
- `mvn-release-local`: members of `<customer>-ont-dep` are allowed to upload to groupId `nl/kadaster/<customer>`
- `mvn-central-remote` (remote is https://repo1.maven.org/maven2)
- `mvn-registry`
- `mvn-legacy-local`: repository with legacy artifacts that are still in use; members of `ontwikkelaars` have permissions to upload
- `mvn-thirdparty-local`: repository uploaded artifacts from others; members of `ontwikkelaars` have permissions to upload
- `mvn-upload-local`: repository for uploaded Kadaster artifacts; members of `ontwikkelaars` have permissions to upload

### Npm
- `npm-snapshot-local`
- `npm-release-local`: Jenkins has permissions to upload
- `npm-remote` (remote is https://registry.npmjs.org)
- `npm-registry`

### NuGet
- `nuget-snapshot-local`: members of `ontwikkelaars` are allowed to upload
- `nuget-release-local`: Jenkins has permissions to upload
- `nuget-remote` (remote is https://nuget.org/)
- `nuget-registry`

### Python
- `python-snapshot-local`: Jenkins and members of `ontwikkelaars` have permissions to upload
- `python-release-local`: Jenkins has permissions to upload
- `python-remote` (remote is https://files.pythonhosted.org/)
- `python-registry`

### Unix
- `unix-release-local`: Jenkins has permissions to upload
- `unix-upload-local`: members of `ontwikkelaars` have permissions to upload
- `unix-registry`
- `unix-legacy-local`

{% include footer.md %}
