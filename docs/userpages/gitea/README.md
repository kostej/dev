{% include header.md %}

## [Gitea (*Planned, not yet operational!*)](https://dev.cloud.kadaster.nl/gitea/)

![GitHub Entities](../../../docs/images/gitea.png)

Kadaster hosts a [Gitea Instance](https://dev.cloud.kadaster.nl/gitea/). [Gitea](https://gitea.io/) is an Open Source Git implementation. 

Within Gitea, we automatically:
- create an Organization for each *Customer*, with webhooks for a specific Jenkins Master
- create a Team for each *Team* with the required permissions to manage repositories
- create an Account for the Jenkins Master 
- add this account to the *Team*

### [Getting started](getting-started.md)
To get started, login using your Kadaster Azure AD credentials. Admins within your team can give you access to *Organizations*. 

### [Kadaster Git Pages for documentation](pages.md)
**Git Pages** can be used to publish documentation for **internal Kadaster** use.

{% include footer.md %}
