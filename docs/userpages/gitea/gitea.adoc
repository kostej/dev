[plantuml]
----
!includeurl ../../../libraries/Archimate.puml
Grouping(GGH,"GiTea") {
    Application_Interface(GHA,"Service Account")
    Application_Component(GHR,"Repository")
    Application_Collaboration(GHT, "Team")
    Application_DataObject(GHO,"Organization")
    Rel_Access_rw(GHA, GHR, "Has access to")
    Rel_Access_rw(GHT, GHR, "Has access to")

    Rel_Aggregation(GHT, GHO, "Belongs to")
    Rel_Aggregation(GHR, GHO, "Belongs to")
}
Business_Actor(BT,"Team")
Application_Service(JM,"Jenkins")
Application_Collaboration(CU, "Customer")
Rel_Access_rw(JM, CU, "Has access to")
Rel_Association(CU, GHO)
Rel_Assignment(CU, BT, "Assigned to")

Rel_Serving(GHT, BT, "Authorizes")
Rel_Serving(JM, BT)
Rel_Serving(GHA,JM, "Authorizes")
Rel_Flow(GHA,JM, "Source Code")
----


