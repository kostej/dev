{% include header.md %}

## [Github Cloud](https://github.com/enterprises/Kadaster)

![GitHub Entities](../../../docs/images/github.png)

Kadaster owns a set of Organizations that are part of [GitHub Enterprise Kadaster](https://github.com/enterprises/Kadaster). Licenses and security policies are managed at this level. The Enterprise manages Organizations. Within all Organization there are Repositories managed by the members of the Organization.

Within Organization [Kadaster-IT](https://github.com/kadaster-it) there are:
- Teams to give members access to a subset of repositories
- GitHub Apps to give Jenkins access to a subset of repositories

### [Getting started](getting-started.md)
Before you can start creating or maintaining code you need a **Github account**. You can use a personal, non-Kadaster account or use an account specific for Kadaster that starts with `kad-`. Go [here](https://github.com/signup?ref_cta=Sign+up&ref_loc=header+logged+out&ref_page=%2F&source=header-home) to create a GitHub-account.

#### [What is the Kadaster Github Enterprise ?](enterprise.md)
The **Kadaster Github Organizations** are organized in the **Kadaster Github Enterprise**. 

#### [Kadaster Github Organizations](organizations.md)
**Kadaster Github repositories** are organized in a number of **Kadaster Github Organizations**. 

#### [Kadaster Github repositories](repositories.md)
Version control of **sourcecode** and collaboration between engineers is realized using **Kadaster Github repositories**. 

#### [Kadaster Github Pages for documentation](pages.md)
**Github Pages** can be used to publish documentation for **internal Kadaster** use.

#### [Using Dependabot and its notifications](dependabot.md)
Dependabot is dependency vulnerability scanning tool integrated into GitHub

#### [Frequently Asked Questions](faq.md)
Frequently asked questions may solve your problem.

#### [External links](links.md)

{% include footer.md %}
