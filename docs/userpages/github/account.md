{% include header.md %}

## Github accounts and requirements

### Kadaster Github Enterprise Cloud access for Kadaster employees  

Create a Github account 

- Kadaster medewerkers vs. Outside collaborator
- Kadaster mail adress.
- Verified email adress.
- SSO.
- Add to organization (wie ?)
- Best practice kad-username

- Inventation Kadaster Enterprise
- Add to team via Henk Everts Waarom niet via group mgt app via IAM (mijn apps).
- 2FA

### Kadaster Github Enterprise Cloud access or non-Kadaster employees

Non-Kadaster employees are granted access to Kadaster Enterprises Organization repositories as **Outside Collaborators** because they have less priviliges.

Such a user will be granted access 

### General Github account requirements:
- The Github account must have 2FA activated.
- The mail addres (preferrably from a known organisation domain).

{% include footer.md %}
