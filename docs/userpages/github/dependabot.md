{% include header.md %}

## Using Dependabot and its notifications 

### What is Dependabot? 

Dependabot is a dependency vulnerability scanning tool, integrated into Github. Its function is to scan dependencies used in your projects for known vulnerabilities and report on them.
Its support is based on package management systems, most notably POM and npm.

There are two ways Dependabot reminds you of security issues in vulnerabilities.
- It scans for known package management files like pom files automatically in your main branch. It will send alerts to contributors/owners and will show a banner on the repository page.
- It can scan for specific package management files and make suggestions via pullrequests to update them accordingly, if they contain vulnerabilities.

For the first one, you don't have to do anything. Team DevForce will enable it for all repositories.
For the second, the pullrequests, you need to manually enable it for your repository. Please follow this guide for information: [here](https://docs.github.com/en/github/administering-a-repository/enabling-and-disabling-version-updates#enabling-dependabot-version-updates)
Keep in mind that branches created by Dependabot might trigger builds in Jenkins or GitHub Actions, depending on your CI/CD setup.

### Managing Dependabot notifications

Dependabot will alert you about vulnerabilities it finds in relevant repositories. This can generate quite a few emails in your inbox initially.
If you are not happy about the way Github sends notifications, it is possible to change these settings. Please read [this guide](https://docs.github.com/en/github/managing-subscriptions-and-notifications-on-github/configuring-notifications) in order to revise your notification settings.

### Quick links
- The [Dependabot website](https://dependabot.com) contains the original Dependabot documentation. It is acquired and integrated by Github so it is free of charge now.
- The [Github documentation](https://docs.github.com/en/github/managing-security-vulnerabilities/managing-vulnerabilities-in-your-projects-dependencies) is worth reading too, notably for Dependabot configuration settings 

{% include footer.md %}
