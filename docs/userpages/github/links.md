{% include header.md %}

# External Github related links

#### Github Infrastructure
- [Github Cloud status](https://www.githubstatus.com){:target="_blank"}  
Obtain the actual status of Github Cloud services and current and past incidents.
- [Kadaster Github Enterprise Cloud (GEC)](https://github.com/enterprises/kadaster){:target="_blank"}  
Github "in the cloud".

#### Github documentation

- [Github documentation](https://docs.github.com){:target="_blank"}  
All official Github documentation.

{% include footer.md %}
