{% include header.md %}

## Kadaster Github Organizations

The [Kadaster Enterprise Github](https://github.com/enterprises/kadaster) environment consists of a number of Github enterprise organizations that are maintained seperately. Having a **Kadaster Github account** does not mean that access is granted to all these organizations automatically. To request access contact  

| Organization | Contact | [2FA required](account.md) | [External collaborators allowed](account.md) | Purpose |
|---|---|---|---|---|
| kadaster | ? | Will be required soon | Yes | Used for sharing Kadaster public sources. |
| kadaster-it | [Contact us](/about/contact.md) | Yes | No | Most Kadaster software is maintained here. |
| kadaster-dev | [Contact us](/about/contact.md) | Yes | No | Team DevForce internal use only. |
| kadaster-lab | [Contact us](/about/contact.md) | Yes | No | For lab purposes of the Emerging Technology Center. |
| pdok | ? | Will be required soon | Yes | All PDOK software is maintained here.
| imbag | ? | Will be required soon | Yes | Used to share BAG Information Models. |

{% include footer.md %}
