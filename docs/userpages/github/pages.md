{% include header.md %}

{% include toc.md %}

# Introduction
When your team maintains one or more **Kadaster services** you may want to publish all the **services' documentation** through one URL using the standard [Github Pages](https://docs.github.com/en/github/working-with-github-pages){:target="_blank"} functionality from Github itself.  

This page describes how you can create a **Github Pages site** in a few minutes from a **Github template repository** containing a **simple Github Pages framework** based on a **Minimal Jekyll theme**. If you use this framework, you only have to create documentation sourcecode in the **develop** branch. After merging it to the **master** branch, the documentation will be published automatically after a few minutes.

> **Best practice**  
Preferably, if the **Github repositories** for **your Kadaster service** follows a strict naming convention, like **abc-\***, create a repository with the name **abc**, in this case https://github.com/kadaster-it/abc. Github pages will be generated automatically from this repository if it is derived from the template.

> The rest of this page assumes **abc** is the name of the documentation repository you want to create. 

# Getting started
You can start a simple Github pages site using the **Github template repository [kadaster-it/dev-documentation-template](https://github.com/kadaster-it/dev-documentation-template){:target="_blank"}** as described hereafter.

## Create the documentation repository 

1. Open a browser and goto [https://github.com/kadaster-it/dev-documentation-template](https://github.com/kadaster-it/dev-documentation-template){:target="_blank"}.
1. Click button **Use this template** to create a new repository.
1. Specify the repository name. In this example **abc**.
1. Provide a **Description** of the contents of the repository.
1. Select **Internal** as the type of the repository. All organization members (Kadaster Employees) should be able to see this repository and the Github Pages site.
1. Select **Include all branches**.
1. Click button **Create repository**.

## Configure the repository settings

1. Goto the root of the Github repository https://github.com/kadaster-it/abc.
1. Click **Settings-\>Options**.
1. Goto section **Github pages** on that page.
1. Select **Master branch / docs folder** as source.
1. Immediately change the **GitHub Pages visibility** into **Private**, otherwise the documentation site will be publicly visible by **anyone on the Internet**. Check the publication **site URL** which should be **https://abc.kadaster-it.github.io**. It takes a few minutes before the site has been generated by Github.
1. Do not change the **Theme** ! 

## Accessibility

Now you have a base documentation site that can be accessed by any **member** of the [https://github.com/kadaster-it](https://github.com/kadaster-it){:target="_blank"} organization. When needed you can add additional **outside collaborators** (give them **read** access to the documentation repository only).

> **Remember**  
By default, **only Kadaster Employees** who are member of the Github organization **kadaster-it** can view the generated documentation pages. 

# Content creation

> **Remember**  
Prepare your documentation in the **develop** branch of the repository. The documentation will be published after your updates have been merged into the **master** branch.

## Tips

- The **Github Pages theme** that is used in this documentation template is [Minimal](https://pages-themes.github.io/minimal/){:target="_blank"}, the code and documentation of this theme can be found on [Github](https://github.com/pages-themes/minimal){:target="_blank"}. Both links provide help on using it.
- The pages you are looking at are an example of how a simple documentation site can look like. You can find the sources in the repository [https://github.com/kadaster-it/dev](https://github.com/kadaster-it/dev){:target="_blank"}.
- Your documentation content must be created using **Github Flavoured Markdown**. For more information see [GFM guide](https://guides.github.com/features/mastering-markdown/){:target="_blank"} and [GFM specification](https://github.github.com/gfm/){:target="_blank"}.
- Some [kramdown](https://kramdown.gettalong.org/quickref.html){:target="_blank"} functions along with [liquid tags](https://shopify.github.io/liquid/){:target="_blank"} are used in the template.
- The site is published using the files below the **/docs** directory in the repository
- To include a header and a footer on a page use as the first and last line of a page:  

```
{% raw %}
{% include header.md %}

....

{% include footer.md %}
{% endraw %}
```

- To include an image showing **Under Construction** on a page ([example](under-construction.md)):  

```
{% raw %}
{% include under-construction.md %}
{% endraw %}
```

- Use a simple hierarchy of files.
- You can edit the `_config.yml` to change the **left logo image** and the title of the pages.
- You can use [emoticons](https://gist.github.com/rxaviers/7360908){:target="_blank"}, like :thumbsup:, in your documentation sources:

```
Use :thumbsup: in your text.
```
- If you want to open a link in a new browser window add \{\:target="_blank"\} to the link definition:

```
{% raw %}
[linkname](url){:target="_blank"}
{% endraw %}
```

{% include footer.md %}
