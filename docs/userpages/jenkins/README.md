{% include header.md %}

## Jenkins
Jenkins is the heart of the Kadaster build, test and deployment infrastructure. Each development team owns a Jenkins Master instance with one or more Agents.

There is a tight dependency with PLP-infrastructure and Active Directory groups. If you are not familiar with PLP, please read the introduction on their [documentation page](https://documentatie.plp.kadaster.nl/docs/).

### Masters
Each team has its own Jenkins Master. Development Services creates and maintains these Masters at your request. Masters themselves can't be used for running the actual builds though, Agents can.

All available Jenkins Masters can be found on [this switchboard page](instances.md). You have read-only access to all masters.

To obtain a Jenkins Master for your team, create an issue in [JIRA - SO/Systeemontwikkeling](https://dev.kadaster.nl/jira/projects/SO) and add the following information:
- Team name (and short name)
- Which PLP-customer(s) should give access (only members of group `<customer>-ont-dep` are authorized)

If you do not own a PLP-customer yet, contact [Hugo Hoitink](mailto:hugo.hoitink@kadaster.nl)

### Agents
Any number of Jenkins Agents can be configured by you. These agents need a build host to run on though.

Supported types are Linux - Docker (PLP-JEDI) and Windows (PLP-SWAN). Request such a build host using [PLP Self-Service](https://portal.plp.kadaster.nl/)

Make sure your Master (`zz_jenkins-<team>`) has access to the proper PLP-environment, through membership of group `<customer>-<environment>-dep`. Otherwise, it won't be capable of accessing the created build host. You can authorize your Master (`zz_jenkins-<team>`) using [PLP Self-Service](https://portal.plp.kadaster.nl/), see https://documentatie.plp.kadaster.nl/docs/howtos/user-koppelen-aan-platformrol/.

Then configure the Agent use [DEV Self-Service](https://pls.kadaster.nl/selfservice/): Navigate to the respective ont environment in the dropdown-box at the top right and click on menu-item `DEVELOP: Jenkins Masters`. Click on the Jenkins Master entry to see or adjust properties.

### Build Tools
There are a lot of [tools](https://www.jenkins.io/doc/book/pipeline/syntax/#tools) available on Jenkins, to perform builds. To see what tools are available follow the link `Pipeline Syntax` on the Jenkins Web-page of a specific pipeline job. Select command 'tool', and a drop-down list will appear with available tools will appear. On usage, Jenkins will download and install these tools.

If a tool requires configuration, this configuration has already been made available.

As an alternative to these tools, any Docker Image (with certain tools pre-installed) can be used to perform the build in. For this, use pipeline statement `docker.image().inside() {}`. Go [here](https://www.jenkins.io/doc/book/pipeline/docker/#advanced-usage-with-scripted-pipeline) for more information on how to use it.

The workspace and tool configuration are available in the docker container started this way.


{% include footer.md %}
