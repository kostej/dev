{% include header.md %}

## Jenkins instance switchboard 

Almost any team has its own Jenkins Master instance. These instances are collected on a switchboard page. You have write access on the Jenkins instance of the team you are working for. You have read-only access to all other instances. This helps collaboration and sharing of knowledge.

| Instance | Team | Go there | Comments |
|:---------|:-----|:--------:|:--------:|
| AKAI | Akte Artificial Intelligence | [:arrow_forward:](https://dev.kadaster.nl/jenkins-akai){:target="_blank"} | - |
| BAG | Basisregistratie Adressen en Gebouwen | [:arrow_forward:](https://dev.kadaster.nl/jenkins-bag){:target="_blank"} | - |
| BGT | Basisregistratie Grootschalige Topografie | [:arrow_forward:](https://dev.kadaster.nl/jenkins-bgt){:target="_blank"} | - |
| BRKBAG | Basis Registratie Kadaster <-> Basisregistratie Adressen en Gebouwen | [:arrow_forward:](https://dev.kadaster.nl/jenkins-brkbag){:target="_blank"} | - |
| BRKINFO | Basis Registratie Kadaster <-> Informatie Voorziening | [:arrow_forward:](https://dev.kadaster.nl/jenkins-brkinfo){:target="_blank"} | - |
| BRTLNG | Basis Registratie Kadaster <-> ? | [:arrow_forward:](https://dev.kadaster.nl/jenkins-brtlng){:target="_blank"} | - |
| CST | Cyber Security Team | [:arrow_forward:](https://dev.kadaster.nl/jenkins-cst){:target="_blank"} | - |
| DBA | Database Administrator Community | [:arrow_forward:](https://dev.kadaster.nl/jenkins-dba){:target="_blank"} | - |
| DEV | Development Services | [:arrow_forward:](https://dev.kadaster.nl/jenkins-prod){:target="_blank"} | - |
| DKK | Digitale Kadastrale Kaart | [:arrow_forward:](https://dev.kadaster.nl/jenkins-dkk){:target="_blank"} | - |
| DSOV | Digitaal Stelsel Omgevingswet Viewer | [:arrow_forward:](https://dev.kadaster.nl/jenkins-dsov){:target="_blank"} | - |
| GAA | Generieke Authenticatie en Authorisatie | [:arrow_forward:](https://dev.kadaster.nl/jenkins-gaa){:target="_blank"} | - |
| GGS | Generieke Geo Services | [:arrow_forward:](https://dev.kadaster.nl/jenkins-ggs){:target="_blank"} | - |
| GIMA | GIS Maatwerk (Zwolle) | [:arrow_forward:](https://dev.kadaster.nl/jenkins-gima){:target="_blank"} | - |
| GMA | GIS Maatwerk (Apeldoorn) | [:arrow_forward:](https://dev.kadaster.nl/jenkins-gma){:target="_blank"} | - |
| GOK | GIS Maatwerk Op Koers | [:arrow_forward:](https://dev.kadaster.nl/jenkins-gok){:target="_blank"} | - |
| GRS | Geometrische Referentie Stelsels | [:arrow_forward:](https://dev.kadaster.nl/jenkins-grs){:target="_blank"} | - |
| GVF | Generieke Vlakgerichte Functionaliteit | [:arrow_forward:](https://dev.kadaster.nl/jenkins-gvf){:target="_blank"} | - |
| HCM | Human Capital Management (SAP) | [:arrow_forward:](https://dev.kadaster.nl/jenkins-hcm){:target="_blank"} | - |
| IAM | Identity and Access Management | [:arrow_forward:](https://dev.kadaster.nl/jenkins-iam){:target="_blank"} | - |
| IFK | InFormatieKaart | [:arrow_forward:](https://dev.kadaster.nl/jenkins-ifk){:target="_blank"} | - |
| IV | Informatie Voorziening | [:arrow_forward:](https://dev.kadaster.nl/jenkins-iv){:target="_blank"} | - |
| JEG | Java Expertise Group | [:arrow_forward:](https://dev.kadaster.nl/jenkins-jeg){:target="_blank"} | - |
| KDP | Kadaster Data Platform | [:arrow_forward:](https://dev.kadaster.nl/jenkins-kdp){:target="_blank"} | - |
| KIKAA | Ketenintegratie Inschrijving Kadaster Automatische Akteverwerking | [:arrow_forward:](https://dev.kadaster.nl/jenkins-kikaa){:target="_blank"} | - |
| KIR | Kadastrale Inwinning en Reconstructie | [:arrow_forward:](https://dev.kadaster.nl/jenkins-kir){:target="_blank"} | - |
| KLIC | Kadaster Leidingen Informatie Centrum | [:arrow_forward:](https://dev.kadaster.nl/jenkins-klic){:target="_blank"} | - |
| KOERS | Kadaster Objecten en Registratie Systeem | [:arrow_forward:](https://dev.kadaster.nl/jenkins-koers){:target="_blank"} | - |
| KOL | Kadaster Online | [:arrow_forward:](https://dev.kadaster.nl/jenkins-kol){:target="_blank"} | - |
| LENK | ? | [:arrow_forward:](https://dev.kadaster.nl/jenkins-lenk){:target="_blank"} | - |
| LV | Landelijke Voorzieningen | [:arrow_forward:](https://dev.kadaster.nl/jenkins-lv){:target="_blank"} | - |
| LVB | Landelijke Voorzieningen Beeldmateriaal | [:arrow_forward:](https://dev.kadaster.nl/jenkins-lvb){:target="_blank"} | - |
| LVBBOD | ? | [:arrow_forward:](https://dev.kadaster.nl/jenkins-lvbbod){:target="_blank"} | - |
| ORKR | Objecten Registratie en Kadastrale Registratie | [:arrow_forward:](https://dev.kadaster.nl/jenkins-orkr){:target="_blank"} | - |
| PDOK | Publieke Dienstverlening Op de Kaart | [:arrow_forward:](https://dev.kadaster.nl/jenkins-pdok){:target="_blank"} | - |
| REC | Reconstructiekaart Kadastrale Grenzen? | [:arrow_forward:](https://dev.kadaster.nl/jenkins-rec){:target="_blank"} | - |
| RNA | Governance, Risk & Compliance tooling? | [:arrow_forward:](https://dev.kadaster.nl/jenkins-rna){:target="_blank"} | - |
| ROO | Ruimtelijke Plannen | [:arrow_forward:](https://dev.kadaster.nl/jenkins-roo){:target="_blank"} | - |
| SNOW | ServiceNOW | [:arrow_forward:](https://dev.kadaster.nl/jenkins-snow){:target="_blank"} | - |
| STG | Stagiaires | [:arrow_forward:](https://dev.kadaster.nl/jenkins-stg){:target="_blank"} | Voor builds van stagiaires |
| TEST | Test Community | [:arrow_forward:](https://dev.kadaster.nl/jenkins-test){:target="_blank"} | - |
| TRE | Terristrische REgistratie | [:arrow_forward:](https://dev.kadaster.nl/jenkins-tre){:target="_blank"} | - |
| VAST | Verbetering Aanleveren STukken | [:arrow_forward:](https://dev.kadaster.nl/jenkins-vast){:target="_blank"} | - |
| VKG | Voorlopige Kadastrale Grenzen en nummeruitgifte | [:arrow_forward:](https://dev.kadaster.nl/jenkins-vkg){:target="_blank"}| - |
| ZANDBAK | Jenkins speeltuin | [:arrow_forward:](https://dev.kadaster.nl/jenkins-zandbak){:target="_blank"} | - |


{% include footer.md %}
