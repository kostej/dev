{% include header.md %}

## Jira
Kadaster uses Jira for issue management to support teams with Scrum and Agile methodologies.

### [Getting started with Jira](getting-started.md)

### [Frequently Asked Questions](faq.md)

{% include footer.md %}
