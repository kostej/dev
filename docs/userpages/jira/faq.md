{% include header.md %}

{% include toc.md %}

# Frequently Asked Questions for Jira

### I cannot login ...

There are 3 (ehm ... 4 !) reasons why Jira refuses to log you in:
1. You provide the wrong password :-) !
1. Your Jira account may be **inactive** when your last login was more than 8 weeks ago.  
Solution: [Contact us](/about/contact.md).
1. Your **Werkplek 2.0 password (Azure Active Directory)** may not be synchronized with **Windows Active Directory** (Jira uses this last Directory).  
Solution: Goto [**Mijn apps**](https://hetkadaster.sharepoint.com/sites/intranet/apps/Paginas/default.aspx) and click app [**Wachtwoord wijzigen**](https://password.iam.kadaster.nl/sspr/private/login). This changes your **Azure Active Directory password**. Your password will be synchronized with the **Windows Active Directory** within a few minutes.
1. Your **Windows Active Directory** account may be locked.  
Solution: Ask the **Kadaster Service Desk** to unlock your **Windows Account Directory account**. 

{% include footer.md %}
