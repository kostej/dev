{% include header.md %}

{% include toc.md %}

# Getting started with Jira as a user

## Get a Jira account

[Contact us](/about/contact.md) to get a Jira account. 

## Basic Jira permissions

Jira is a **collaboration tool** and has been setup **very open**. These basic permissions should NOT be restricted by **Jira Project Administrators** ! Jira users have the following basic permissions:
1. You can **view** (almost) any **Jira issue** in **any Jira Project**.
1. You can **add comments** to (almost) any **Jira issue** in **any Jira Project**.
1. You can **create Jira issues** in (almost) **any Jira Project**.

## Jira Projects
Most of the time a **Jira user** collaborates with a **Kadaster team**. This team has access to 1 or more **Jira Projects**. You need to be added to these Jira Project(s) as a **Developer**. The **Developer** role grants the user the permission to work with Jira issues (change attributes and move issues through the Jira Project workflow). The **Developer** role is granted to you by one of the **Jira Project Administrators** within your team. This is **team selfservice**.

## More info needed ?
Ask your team members. They will be glad to explain the basic features of Jira to you.

# Getting started with Jira as a team

## Get a Jira Project

[Contact us](/about/contact.md) to get a **Jira Project**.

A **Jira Project** is never a short living object. Therefore, you have to explain the need for the Jira Project to us. A request is sometimes rejected. 

You need to provide the following information:

1. A **short description of the Jira Project**.
1. The **Jira Project Key** which is derived from the short description. This is a 2 to 5 character key which will be the prefix for all **Jira issues** in the **Jira Project**. This makes Jira issues, together with a sequence number uniquely identifiable. 
1. The desired **Jira issue workflow**. 
1. The names of 2 **Jira Project Administrators**.

## Maintenance of a Jira Project

**Jira Project Administrators** maintain access to Jira Project: 

1. They maintain the **Jira Project** roles. WARNING: do not modify the standard Jira groups that come with the Jira Project ! You may end up with a Jira Project that disappears from the Jira Project overview. Normally, you only need to add and remove users o/from the **Developer** role. 

## Jira Boards
Anyone can create Scrum and Kanban boards. This is a view on (a subset) of Jira issues in one or more Jira Projects. Boards may help to get the work done.

{% include footer.md %}
