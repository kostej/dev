{% include header.md %}

## Miscellaneous tools and solutions

### [Publishing static HTML](static-html.md)
Some tools generate static HTML. Learn how to publish these pages.

### [TiCS code scanning](tics.md)
Tiobe TiCS is a formal alternative to SonarQube.

{% include footer.md %}
