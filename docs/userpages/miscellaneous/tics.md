{% include header.md %}

## Tiobe TiCS (Beta)

### Introduction
At Kadaster, SonarQube is used to measure Software Quality. Sometimes however, other Government organisations ask to provide an **independent indicator of software quality**. This is where [Tiobe](https://tiobe.com) comes in. 

Tiobe, as a company, measures **software code quality** in a standardized way which results in a [Tiobe Quality Indicator (TQI)](https://www.tiobe.com/tqi/definition). In the past few years, Tiobe has performed [Software Quality Assessments](https://www.tiobe.com/products/quality-assessment/) for Kadaster on request for certain Kadaster components. This is a tedious process which costs a lot of money and manual processing time. Tiobe uses the [Software Quality Framework TiCS (Tiobe Code Scanning)](https://www.tiobe.com/tics/tics-framework/) for its Software Quality Assessment.

TiCS is now available as a SaaS (Software as a Service). The TiCS SaaS has been developed by Tiobe upon request by Kadaster and finalized in cooperation with team DevForce. A rudimentary SaaS is available now which can be used in Jenkins pipelines. 

### Quick links
- Goto the [Tiobe website](https://tiobe.com) for general information.
- The [Tiobe documentation](https://www.tiobe.com/documentation/) provides general information on how Tiobe looks at Software Code Quality. 
- The [Tics Fact Sheet](https://www.tiobe.com/tics/fact-sheet/) contains the list of supported languages and underlying codescanning tools for each language. TiCS integrates the scanning results of all these tools to provide a uniform view of the software code quality.
- The [TiCS Framework](https://www.tiobe.com/tics/tics-framework/) describes the base TiCS components. The TiCS Saas has been developed around these components.
- The [TiCS documentation](https://portal.tiobe.com/2020.4/docs) extensively describes the TiCS Framework, all its configuration options and [code scanning metrics](https://portal.tiobe.com/2020.4/docs/#doc=user/enduser.html%23CommandLineInterface).
- The [TiCS live demo](https://demo.tiobe.com/) shows the code scanning results of some Tiobe demo projects. Please take a look !

## TiCS SaaS and Jenkins integration
### Introduction 
The TiCS SaaS is not described (yet) in the TiCS documentation. Therefore, this page will describe some high level implementation details for a better understanding of how the solution has been integrated with Kadaster Jenkins (Agent) instances.

### TiCS SaaS Components
The TiCS SaaS components are hosted by [Acknowledge](https://www.acknowledge.nl), a dutch service provider. The TiCS SaaS consists of the following components:
- **TiCS Database**  
The database contains the results of TiCS Code Scanning activities which can be viewed using the TiCS Viewer.
- **TiCS Viewer**  
A webserver providing access to the TiCS Database to view the results of TiCS Code Scanning activities.
- **TiCS Client software repository**  
The TiCS Client software used by a Jenkins agent is dynamically downloaded from this repository. This repository is proxied by the Kadaster Artifactory instance.

### TiCS Jenkins Components
A number of Jenkins TiCS components have been developed by team DevForce to implement secure communication between the TiCS SaaS backend and Kadaster Jenkins Agents and to simplify TiCS analysis code in Jenkins pipelines: 
- **Jenkins Agent SSH Tunnel**  
A Jenkins Agent container maintains an SSH tunnel with the remote TiCS SaaS. The SaaS only accepts network traffic originating from the Kadaster ssl-proxy host using a dedicated firewall rule. **Your Jedi-2 Jenkins agent(s) must be updated to run this SSH tunnel**.
- **Jenkins TiCS Custom Tool**  
A Jenkins Custom Tool is provided to hide the details of installing the TiCS Client tools that are needed to perform code scanning. The activation is hidden in the *Jenkins TiCS Analysis library* which is used in your Jenkins pipeline.  
- **Jenkins TiCS Analysis library**  
A global Jenkins Groovy library is provided to hide the details of TiCS code scanning in your Jenkins pipeline. This library contains only one function called *runTicsAnalysis*.
- **Jenkins TiCS global library configuration**  
Implements the activation of the *Jenkins Tics Analysis library* in Jenkins as a global Jenkins Groovy library so no library import is necessary in your pipeline code. 

## TiCS code scanning in Jenkins pipelines

### Update your JEDI-2 Jenkins agent(s)

Before a TiCS analysis can be run successfully, your **JEDI-2 Jenkins agent(s)** must be updated, otherwise there's no SSH tunnel active between Jenkins Agent and the TiCS SaaS backend. Without a SSH channel the TiCS client tools cannot communicate with the remote TiCS database.

For updating your JEDI-2 Jenkins agents, please refer to the PLS Selfservice portal.

### Required Jenkins Pipeline code
Performing a TiCS code scan is just as easy as adding a step in your Jenkins pipeline containing the following groovy code:

```groovy
stage('TICS code scan') {
    steps {
        // Warning: TiCS code scanning at Kadaster can only be executed on JEDI-2 Jenkins agents !
        runTicsAnalysis("projectname","branchname","metrics","excluded metrics","path")
    }
}
```

The Groovy function `runTicsAnalysis` has 5 parameters:
1. **Projectname**, a unique name in the global namespace of the TiCS Viewer. The best practice is to use the name of the Github repository you're going to scan. E.g. "dxp-hello-world-advanced-backend". Use a least "customer-\<unique string\>" to not interfere with TiCS projects from other teams.
1. **Branchname**, the branch name of the repository to scan. E.g. "master" or "develop".
1. **Metrics**, the metrics used to scan the repository sourcecode. E.g. "ALL" (the default). You can define your own set of metrics of course if you are not interested in the totalrating of your code (in this case your code will be rated "F"). Use "ALL" if the rating is important to you. This behaviour forces your code to obey the complete set of metrics that Tiobe has defined. Possible *metrics* values can be found [here](https://portal.tiobe.com/2020.4/docs/#doc=user/enduser.html%23CommandLineInterface).
1. **Excluded metrics**, metrics to be excluded in the code scanning activity. E.g. "" (the default). Possible values can be found [here](https://portal.tiobe.com/2020.4/docs/#doc=user/enduser.html%23CommandLineInterface). Use "" otherwise your code will also be rated "F" ! This behaviour forces your code to obey the complete set of metrics that Tiobe has defined. 
1. **Path**, the path relative to the Jenkins workspace where your code resides. E.g. "." (the home directory of the Jenkins workspace, which is the default). 

After a successfull code scan the results are stored in the Tics Database which can be viewed with the Tics Viewer. 

> :warning: The Groovy function `runTicsAnalysis` initializes a number of TiCS configuration files. Do not ever, ever, change the content of these files in your pipeline. It may result in errors, or worse, in loss of data in the TiCS database. **Contact team DevForce if you need TiCS configuration changes**.

### Viewing the TiCS code scanning results
TiCS code scanning results can be viewed using the [Kadaster Tics Viewer](https://acc.kadaster-tics.tiobe.com/tiobeweb/TICS). It looks like SonarQube.

Notes on using the TiCS Viewer:
- :warning: Due to firewall constraints in the TiCS SaaS, only network traffic that originates from the Kadaster Proxy hosts is accepted. Therefore, you can *only successfully login* when your laptop is connected to the internal Kadaster network using a VPN. 
- :warning: There is no TiCS Viewer user management yet. To be specified when the TiCS status is **production**.

### Example Github repositories and Jenkins pipelines
To be specified when the TiCS status is **production**. 

{% include footer.md %}
