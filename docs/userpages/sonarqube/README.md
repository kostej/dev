{% include header.md %}

## [SonarQube](https://dev.kadaster.nl/sonar/)

SonarQube facilitates static code scanning and publishing the results

### Projects
Projects in SonarQube contain the metrics of a set source code.

The *Project Key* must start with `nl.kadaster.<customer>`.

### Permissions
Members of group `<customer-ont-dep>` have Administrator permissions on projects with *Project Key* that start with `nl.kadaster.<customer>`. 

### Housekeeping
SonarQube doesn't keep the results of a scan forever: certain rules apply to the housekeeping that is performed on projects and metrics within them:
- *Keep only one analysis a day after 24 hours*: after this number of hours, if there are several analyses during the same day, the DbCleaner keeps the most recent one and fully deletes the other ones.
- *Keep only one analysis a week after 4 weeks*: after this number of weeks, if there are several analyses during the same week, the DbCleaner keeps the most recent one and fully deletes the other ones
- *Delete all analyses after 52 weeks*: after this number of weeks, all analyses are fully deleted.

{% include footer.md %}
