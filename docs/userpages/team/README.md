{% include header.md %}

![DevOps](/images/devops.jpg)

Team **Development Services / DevForce** includes the following members:

- [Hugo Hoitink](https://eur.delve.office.com/?u=d33c53a2-5c44-4bc2-8750-3392be32f0be&v=work){:target="_blank"} - Product Owner
- [Bert Meedendorp](https://eur.delve.office.com/?u=1381ffcf-6fd2-4817-97e9-c6d280b64fff&v=work){:target="_blank"}
- [Hans Koster](https://eur.delve.office.com/?u=ed052e19-3797-4b6f-bef9-80dbb5beb0ae&v=work){:target="_blank"}
- [Nico Pranger](https://eur.delve.office.com/?u=1f515221-17ed-47f5-8e68-bf92ecbbba7b&v=work){:target="_blank"}
- [Harold-Jan Vuyk](https://eur.delve.office.com/?u=4cbe6e8a-b827-4a8c-8ea3-094892497252&v=work){:target="_blank"}
- [Richard Beumer](https://eur.delve.office.com/?u=39976711-8214-44c8-9eb8-b4cede629832&v=work){:target="_blank"}
- [Thijs Cramer](https://eur.delve.office.com/?u=619de057-e507-4b44-b846-52750ca47543&v=work){:target="_blank"}
- [Kevin Hensel](https://eur.delve.office.com/?u=425f2d5a-6cf8-4dbb-a8cc-644e339567d6&v=work){:target="_blank"}
- [Luuk Tersmette](https://eur.delve.office.com/?u=85349627-59c4-4b21-a243-d0825153e1bd&v=work){:target="_blank"}
- [Corneel van Holland](){:target="_blank"}

## Do you have a question, request or problem ?

### Try this first ....
There are several ways to get an answer. Please consider the following before contacting us:

1. Google is your friend.
1. Look into this [Development Services documentation site](/).
1. Ask colleagues in your own team or other teams, they are willing to help you.
1. Ask your question in the Slack community you belong to, there are several channels in the [Kadaster Slack workspace](https://app.slack.com/client/T09N2K2FQ/browse-channels){:target="_blank"}.
1. Ask your question in the [Kadaster Slack channel "Dev"](https://kadaster-it.slack.com/archives/C7U4S9S3G){:target="_blank"} or [Kadaster Slack channel "Developers"](https://kadaster-it.slack.com/archives/C0MGN3JBF){:target="_blank"}
).

As a last resort contact the [DevForce team](/userpages/team), read on.

### .. and then contact us

Depending on your question or problem, there is a preferred way of contacting us:

- A simple question about Development Services tooling may be asked in the [Kadaster Slack channel "Dev"](https://kadaster-it.slack.com/messages/C7U4S9S3G){:target="_blank"}, we, or fellow developers, may help you instantly.
- For simple requests and problems you can use our team mailbox: [soo@kadaster.nl](mailto:soo@kadaster.nl), e.g. for an account, etc. This mailbox is checked regularly by all team members. Please do not mail team members personally because they may be out of the office so your question may not be answered. 
- If you have a bigger problem or a (functional) request regarding our Development Services you can create a [Jira](https://dev.kadaster.nl/jira){:target="_blank"} issue in [Jira project "Systeemontwikkeling"](https://dev.kadaster.nl/jira/projects/SO){:target="_blank"}. Your request is added to our backlog.  
- WARNING: **ServiceNow** is not a preferred way to initiate an incident for our team. We do not use ServiceNow very actively and therefore it may take longer before your question reaches us.

{% include footer.md %}
